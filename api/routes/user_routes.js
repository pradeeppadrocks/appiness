const express = require('express')
const router = express.Router()
const userController = require('../controllers/user_controler')

/*
    Registration of user Routes

    For checking got to postman

    Method : POST 
    URL:  http://localhost:3000/user/signup/

    parameter:

    {
        "email": "admin@gmail.com",
        "password":"admin"
    }


    Note*: First registered user is admin and others are user

*/
router.post('/signup', userController.signup)

/*
    Login of user Routes

    For checking got to postman

    Method : POST 
    URL:  http://localhost:3000/user/login/

    parameter:

    {
        "email": "admin@gmail.com",
        "password":"admin"
    }


    note*: After login you will get access token .this token you have to pass in headers for other routes
*/
router.post('/login', userController.login)

module.exports = router