const express = require('express')
const router = express.Router()

const checkAuth = require('../middleware/check-auth')
const categoryControler = require('../controllers/category_controler')

/*
    Creating category

    For checking got to postman

    In header you have to pass autherization like below

    Authorization Bearer "add login token here"

    Method : POST 
    URL:  http://localhost:3000/category/

    parameter:

   {
    "categoryName":"Lays"
   }

*/
router.post('/', checkAuth, categoryControler.create_Category)


/*
    Get ALL category

    For checking got to postman

    In header you have to pass autherization like below

    Authorization Bearer "add login token here"

    Method : GET 
    URL:  http://localhost:3000/category/

*/

router.get('/', checkAuth, categoryControler.get_Category)


/*
    Get category with products

    For checking got to postman

    In header you have to pass autherization like below

    Authorization Bearer "add login token here"

    Method : GET 
    URL: http://localhost:3000/category/5d3d494a0bffe654b432744d

*/
router.get('/:categoryId', checkAuth, categoryControler.get_Category_Of_Products)


/*
    Delete category . IF you delete category its associated product will also delete

    For checking got to postman

    In header you have to pass autherization like below

    Authorization Bearer "add login token here"

    Method : DELETE 
    URL: http://localhost:3000/category/5d3d494a0bffe654b432744d

*/
router.delete('/:categoryId', checkAuth, categoryControler.delete_Category_Of_Products)

module.exports = router

