const express = require('express')
const router = express.Router()

const checkAuth = require('../middleware/check-auth')
const productController = require('../controllers/product_controler')


/*
    Creating Products

    For checking got to postman

    In header you have to pass autherization like below

    Authorization Bearer "add login token here"

    Method : POST 
    URL:  http://localhost:3000/products/

    parameter:

   {
    "name":"paddy 11111",
    "price":22,
    "categoryId":"5d3d49550bffe654b432744e"
   }

*/
router.post('/', checkAuth, productController.create_products)


/*
    getting all Products

    For checking got to postman

    In header you have to pass autherization like below

    Authorization Bearer "add login token here"

    Method : GET 
    URL:  http://localhost:3000/products/

    parameter:


*/
router.get('/', checkAuth, productController.get_All_Products)

module.exports = router