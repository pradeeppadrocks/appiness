const Product = require('../models/product_model')
const mongoose = require('mongoose')
const Gen = require('../modules/module.generic')

let productData = {
    create_products: create_products,
    get_All_Products: get_All_Products
}

//creating products
function create_products(req, res, next) {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        categoryId: req.body.categoryId
    })

    Product.find({ name: req.body.name })
    .exec()
    .then(user => {
        if (user.length >= 1) {
            return res.json(Gen.responseReturn(409, 'product Already Exists Try Different product', {}))
        } else {
            product.save().then(result => {
                let product = {
                    name: result.name,
                    price: result.price,
                    _id: result._id,
                    categoryId: result.categoryId,
                }
                return res.json(Gen.responseReturn(200, 'product was created', product, true))
            }).catch(err => {
                return res.json(Gen.responseReturn(500, err, {}))
            })
        }
    })
}


//getting all products
function get_All_Products(req, res, next) {
        Product.find()
        .select('name price _id  categoryId')
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                products: docs.map(prod => {
                    return {
                        _id: prod._id,
                        name: prod.name,
                        price: prod.price,
                        categoryId: prod.categoryId
                    }
                })
            }
            if (docs.length >= 0) {
                res.status(200).json(response)
            } else {
                res.status(404).json({
                    message: 'No entries found'
                })
            }
        })
        .catch(err => {
            res.status(500).json({
                message: 'something went wrong',
                error: err
            })
        })
}

module.exports = productData