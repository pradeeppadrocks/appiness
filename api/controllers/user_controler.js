const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const User = require('../models/user_model')
const constValue = require('../../constants')
const Gen = require('../modules/module.generic')

let userData = {
    signup: signup,
    login: login
}

//login 
function login(req, res, next) {
    //checking email is there or not
    User.find({ email: req.body.email })
        .exec()
        .then(user => {
            //email not there sending auth msg bcz hacker or someone are not able to get this email exists or not when they are trying to login
            if (user.length < 1) {
                return res.json(Gen.responseReturn(401, 'Authentication Failed', {}))
            }
            //comparing password
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.json(Gen.responseReturn(401, 'Authentication Failed', {}))
                }
                if (result) {
                    const token = jwt.sign({
                        email: user[0].email,
                        _id: user[0]._id
                    }, constValue.env.JWT_KEY,
                        {
                            expiresIn: "5h"
                        }
                    )
                    let loginRes = {
                        access_token: token,
                        role: user[0].role
                    }
                    return res.json(Gen.responseReturn(200, 'Login Success', loginRes, true))
                }
                return res.json(Gen.responseReturn(401, 'Authentication Failed', {}))
            })
        })
        .catch(err => {
            res.status(500).json({
                message: 'aaa',
                error: err
            })
        })
}

//signup
function signup(req, res, next) {
    //checking email already exists or not
    User.find({ email: req.body.email })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                return res.json(Gen.responseReturn(409, 'Email Already Exists Try Different Email', {}))
            } else {
                //first time register is admim by setting role=1
                let role = 2
                User.find()
                    .exec()
                    .then(user1 => {
                        if (user1.length >= 1) {

                            role = 2
                        } else {
                            role = 1
                        }
                    })

                //password hashing done here
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.json(Gen.responseReturn(500, err, {}))
                    } else {
                        const user = new User({
                            _id: mongoose.Types.ObjectId(),
                            email: req.body.email,
                            password: hash,
                            role: role
                        })
                        //saving the data
                        user.save()
                            .then(result => {
                                return res.json(Gen.responseReturn(201, 'Registered Successfully', {}, true))
                            })
                            .catch(err => {
                                return res.json(Gen.responseReturn(500, err, {}))
                            })
                    }
                })
            }
        })
}

module.exports = userData