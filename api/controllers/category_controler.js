const Category = require('../models/category_model')
const Product = require('../models/product_model')
const Gen = require('../modules/module.generic')

const mongoose = require('mongoose')

let categoryControler = {
    create_Category: create_Category,
    get_Category_Of_Products: get_Category_Of_Products,
    delete_Category_Of_Products: delete_Category_Of_Products,
    get_Category: get_Category
}

//creating category
function create_Category(req, res, next) {
    const category = new Category({
        _id: new mongoose.Types.ObjectId(),
        categoryName: req.body.categoryName
    })
    Category.find({ categoryName: req.body.categoryName })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                return res.json(Gen.responseReturn(409, 'categoryName Already Exists Try Different categoryName', {}))
            } else {
                category.save().then(result => {
                    let category = {
                        categoryName: result.name,
                        _id: result._id
                    }
                    return res.json(Gen.responseReturn(200, 'Category created', category, true))
                }).catch(err => {
                    res.status(500).json(err)
                })
            }
        })
}


//Getting all Category list
function get_Category(req, res, next) {
    Category.find()
        .select('  _id  categoryName')
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                category: docs.map(prod => {
                    return {
                        _id: prod._id,
                        categoryName: prod.categoryName,
                    }
                })
            }
            if (docs.length >= 0) {
                res.status(200).json(response)
            } else {
                res.status(404).json({
                    message: 'No entries found'
                })
            }
        })
        .catch(err => {
            res.status(500).json({
                message: 'something went wrong',
                error: err
            })
        })
}

//getting category with products
function get_Category_Of_Products(req, res, next) {
    const id = req.params.categoryId
    Product.find({ categoryId: id })
        .populate('Category')
        .exec().
        then(order => {
            if (order) {
                let len = order.length
                res.status(200).json({
                    count: len,
                    order: order

                })
            } else {
                res.status(404).json({
                    message: 'No valid entry found'
                })
            }
        })
        .catch(err => {
            res.status(500).json(err)
        })
}

//deleting category with all related products also
function delete_Category_Of_Products(req, res, next) {
    const id = req.params.categoryId
    Category.deleteOne({ _id: id })
        .exec()
        .then(result => {
            Product.deleteMany({ categoryId: id })
                .exec()
                .then(result2 => {
                    res.status(200).json(result2)
                })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

module.exports = categoryControler