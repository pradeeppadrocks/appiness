const express = require('express')
const app = express()
const config = require('./config/database.config')

const morgan = require('morgan')
const bodyParser = require('body-parser')
const cors=require('cors')

 const mongoose=require('mongoose')


const productRoutes = require('./api/routes/products_routes')
const userRoutes=require('./api/routes/user_routes')
const categoryRoutes=require('./api/routes/category_routes')

// mongoose.connect('mongodb+srv://node-rest-shop:'+ process.env.MONGO_ATLAS_PW +'@node-rest-shop-rr95l.mongodb.net/test?retryWrites=true&w=majority',{
//      useNewUrlParser: true 
// })

/* mongoose.connect('mongodb+srv://node-rest-shop:node-rest-shop@node-rest-shop-rr95l.mongodb.net/test?retryWrites=true&w=majority',{
     useNewUrlParser: true 
})
 */
mongoose.connect(config.db.uri, { useNewUrlParser: true }, err => {
    if (err) {
        console.log('Error occured:', err)
    } else {
        console.log("Connected to mongodb")
    }
})
app.use(cors())

app.use(morgan('dev'))
app.use('/uploads',express.static('uploads'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT,POST,PATCH,GET')

        return res.status(200).json({})
    }
    next()
})



app.use('/products', productRoutes)
app.use('/user', userRoutes)
app.use('/category',categoryRoutes)



app.use((req, res, next) => {
    const error = new Error('Not Found')
    error.status = 404
    next(error)
})

app.use((error, req, res, next) => {
    res.status(error.status || 500)
    res.json({
        error: {
            message: error.message
        }
    })
})

module.exports = app